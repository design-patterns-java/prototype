package org.jonasrodrigues;

import org.jonasrodrigues.Vehicles.Vehicle;

public class Main {
    public static void main(String[] args) {
        System.out.println("Prototype Pattern!");
        Vehicle vehicle = getVehicle();

        Vehicle newVehicle = Vehicle.Builder.from(vehicle)
                .plate("YOU6600")
                .renavam("64279888519")
                .build();
    }

    public static Vehicle getVehicle() {
        return Vehicle.Builder.create("VW", "Gol", "White")
                .plate("NCC6472")
                .renavam("44279863219")
                .build();
    }

}
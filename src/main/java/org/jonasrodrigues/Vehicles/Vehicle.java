package org.jonasrodrigues.Vehicles;

import lombok.Getter;

@Getter
public class Vehicle {

    private String brand;
    private String model;
    private String color;
    private String plate;
    private String renavam;

    private Vehicle() {}
    public static class Builder {

        private Vehicle entity = new Vehicle();

        public static Builder create(String brand, String model, String color) {
            Builder builder = new Builder();
            builder.brand(brand).model(model).color(color);
            return new Builder();
        }

        public static Builder from(Vehicle vehicle) {
            Builder builder = new Builder();
            return builder.brand(vehicle.brand)
                    .model(vehicle.model)
                    .color(vehicle.color);
        }

        private Builder brand(String brand) {
            entity.brand = brand;
            return this;
        }

        private Builder model(String model) {
            entity.model = model;
            return this;
        }

        private Builder color(String color) {
            entity.color = color;
            return this;
        }

        public Builder plate(String plate) {
            entity.plate = plate;
            return this;
        }

        public Builder renavam(String renavam) {
            entity.renavam = renavam;
            return this;
        }

        public Vehicle build() {
            return entity;
        }

    }

}
